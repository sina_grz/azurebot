import requests
import static
from datetime import datetime
Baseurl = 'http://devops.pshk.ir:8080'

payload = {}
base_headers = {
  'Authorization': static.Token
}
class workitems():

    def get_workitems(self,directive,ids):
        response = requests.request("GET", Baseurl+'/{}/_apis/wit/workitems?ids={}&api-version=5.0'.format(directive,ToUrlArray(ids)), headers=base_headers, data = payload)
        print(Baseurl+'/{}/{}/_apis/wit/workitems?ids={}&api-version=5.0'.format(directive[0],directive[1],ToUrlArray(ids)))
        
        return response.json()

    def add_tags(self,directive,id,tags):
        body = "[{\"op\": \"add\",\"path\": \"/fields/System.Tags\",\"value\": \""+'; '.join(tags)+ '"'+"}]"
        headers = base_headers
        headers['Content-Type'] = 'application/json-patch+json'
        print(body)
        response = requests.request("PATCH", Baseurl+'/{}/_apis/wit/workitems/{}?api-version=5.0'.format(directive,id), headers=headers, data = body)
        #print(response.status_code,Baseurl+'/{}/_apis/wit/workitems/{}?api-version=5.0'.format(directive,id))
        
    def get_workitem_updates(self,directive,id):
        response = requests.request("GET", Baseurl+'/{}/_apis/wit/workitems/{}/updates?api-version=5.0'.format(directive,id), headers=base_headers, data = payload)
        return response.json()

    def get_qurey_result(self,directive,id):
        response = requests.request("GET",Baseurl+'/{}/_apis/wit/wiql/{}?api-version=5.0'.format(directive,id),headers=base_headers, data = payload)
        return response.json()

def ToUrlArray(array):
    return array.__str__().replace("[",'').replace(' ','').replace(']','')
    

