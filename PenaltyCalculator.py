from AzureClient import *
import static
from time import sleep
from datetime import datetime
from utils import *
from openpyxl import *
import sys, getopt
import argparse
from pytz import *

StartDate = datetime(2019,1,1,tzinfo=timezone('Iran'))
EndDate = datetime(2030,1,1,tzinfo=timezone('Iran'))
"""
def set_val(args):
    global StartDate,EndDate
    StartDate = args.StartDate
    EndDate = args.EndDate
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
set_value.add_argument('--StartDate',default='datetime(2019)')
set_value.add_argument('--EndDate',default=datetime(2030))
set_value.set_defaults(func=set_val)
args = parser.parse_args()
"""
#args.func(args)

workbook = load_workbook(static.InputPath)
sheet = workbook.active
names = list()
rrp = list()
penalty = list()
rperiod = list()
for row in sheet.iter_rows(values_only=True):
    if(row[0] != None):
        names.append(row[0])
        rrp.append((int(row[1]),int(row[2]),int(row[3]),int(row[4])))
        penalty.append(int(row[5]))
        rperiod.append(row[6].split('-'))
workbook.close()

workbook = Workbook()
sheet = workbook.active

workitems = workitems()
IgnoreStates = ['Closed','Resolved','Removed']
targets = workitems.get_qurey_result('DefaultCollection/Staff Works', static.CalculatorQueryId)['workItems']
row = 1
for item in targets:
    task = workitems.get_workitems('DefaultCollection/Staff Works', [item['id']])
    priority = int(task['value'][0]['fields']['Microsoft.VSTS.Common.Priority'])
    print(priority)
    updates = workitems.get_workitem_updates('DefaultCollection', item['id'])
    creator = task['value'][0]['fields']['System.CreatedBy']['uniqueName']
    asssighnment = task['value'][0]['fields']['System.AssignedTo']['uniqueName']
    lastupdate = TimeConverter(task['value'][0]['fields']['System.CreatedDate'])
    taskstate = updates['value'][0]['fields']["System.State"]["newValue"]
    ind = 0
    for update in updates['value']:
        ind += 1
        if(update['fields'].keys().__contains__('System.AssignedTo')):
            asssighnment = update['fields']['System.AssignedTo']['newValue']['uniqueName']
        
        if(names.__contains__(asssighnment) and not(IgnoreStates.__contains__(taskstate))):
            index = names.index(asssighnment)
            if(not(update['fields'].keys().__contains__('System.Tags') and len(update['fields'].keys()) <= 6) ):
                currenttime = TimeConverter(update['fields']['System.ChangedDate']['newValue'])
                et = CalculateEffectiveTime(rperiod[index],lastupdate,currenttime)
                PenaltyStartDate = CalculatePenaltyDate(rperiod[index],et - (rrp[index][priority -1] * 3600),currenttime)
                include = False
                if(currenttime > StartDate and PenaltyStartDate < EndDate ):
                    if(PenaltyStartDate < StartDate):
                        EfectiveTime = CalculateEffectiveTime(rperiod[index],StartDate,currenttime)
                    else:
                        EfectiveTime = et - (rrp[index][priority-1] * 60 * 60 )
                    if(currenttime > EndDate):
                        EfectiveTime = CalculateEffectiveTime(rperiod[index],currenttime,EndDate)
                    else:
                        print(lastupdate,currenttime,et / 3600)
                        EfectiveTime = et - (rrp[index][priority-1] * 60 * 60 )
                    include = True
                
                if(et > (rrp[index][priority-1] * 3600) and include):
                    print(et,lastupdate,currenttime,CalculatePenaltyDate(rperiod[index],et - rrp[index][priority -1],currenttime))
                    PenaltyStartDate = CalculatePenaltyDate(rperiod[index],et - (rrp[index][priority -1] * 3600),currenttime)
                    sheet.cell(row,1).value = int(item['id'])
                    sheet.cell(row,2).value = asssighnment
                    sheet.cell(row,3).value = lastupdate.strftime('%Y-%m-%d %H:%M:%S')
                    sheet.cell(row,4).value = PenaltyStartDate.strftime('%Y-%m-%d %H:%M:%S')
                    sheet.cell(row,5).value = currenttime.strftime('%Y-%m-%d %H:%M:%S')
                    sheet.cell(row,6).value = EfectiveTime / 3600
                    sheet.cell(row,7).value = EfectiveTime * (penalty[index] / 60 / 60)
                    row += 1
                    workbook.save(static.OutputPath)
                lastupdate = currenttime
                if(update['fields'].__contains__('Microsoft.VSTS.Common.Priority')):
                    priority = int(update['fields']['Microsoft.VSTS.Common.Priority']['newValue'])
                if(update['fields'].keys().__contains__('System.State')):
                    taskstate = update['fields']["System.State"]["newValue"]
    currenttime = datetime.now(tz=timezone('Iran'))
    if(currenttime > EndDate):
        currenttime = EndDate
    et = CalculateEffectiveTime(rperiod[index],lastupdate,currenttime)
    if(et > (rrp[index][priority-1] * 3600) and not(IgnoreStates.__contains__(taskstate))):
        print(et, lastupdate, currenttime, CalculatePenaltyDate(rperiod[index], et - rrp[index][priority - 1], currenttime))
        sheet.cell(row, 1).value=int(item['id'])
        sheet.cell(row, 2).value= asssighnment
        sheet.cell(row, 3).value= lastupdate.strftime('%Y-%m-%d %H:%M:%S')
        sheet.cell(row, 4).value= CalculatePenaltyDate(rperiod[index],et - (rrp[index][priority -1] * 3600),currenttime).strftime('%Y-%m-%d %H:%M:%S')
        sheet.cell(row, 5).value= currenttime.strftime('%Y-%m-%d %H:%M:%S')
        sheet.cell(row, 6).value=  (et - (rrp[index][priority-1] * 60 * 60 )) / 3600
        sheet.cell(row, 7).value= ((et - (rrp[index][priority-1] * 60 * 60 )) / 3600) * (penalty[index] / 60 / 60)
        row += 1

workbook.save(static.OutputPath)
